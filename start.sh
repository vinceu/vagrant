#!/bin/bash
parted -s -a optimal /dev/sdb mklabel gpt -- mkpart primary ext4 1 -1
mkfs.ext4 /dev/sdb1
mount /dev/sdb1 /mnt

